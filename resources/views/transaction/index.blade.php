@extends('template.main')
@section('title', 'Transaction')


@section('content')

    <div class="card-header">
        <h3 class="card-title"><strong>Table Data Transaction</strong></h3>
        <div class="col-12 mt-5">
            <a href="{{ url('/transaction/create') }}" class="btn btn-primary" role="button">
                Tambah data Transaction
            </a>
        </div>
        @if (session('status'))
            <div class="row mt-2">
                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <table class="table table-bordered">
        <thead>
            <tr class="text-center">
                <th style="width: 10px">No</th>
                <th>Payment</th>
                <th>Address</th>
                <th>Total Price</th>
                {{-- <th>Status</th> --}}
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transaction as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->payment->bank_name }}</td>
                    <td>{{ $item->address }}</td>
                    <td>{{ $item->total_price }}</td>
                    {{-- <td>{{ $item->status }}</td> --}}
                    <td>
                        <form method="POST" action="{{ url('/transaction/' . $item->id) }}">
                            @csrf
                            @method('delete')
                            <a href="{{ url('/transaction/') }}" class="btn btn-success btn-sm">Detail</a>
                            <a href="{{ url('/transaction/' . $item->id) . '/edit' }}"
                                class="btn btn-info btn-sm">Edit</a>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                    {{-- <td>
                        <a class="btn btn-primary" href="#" role="button">Link</a>
                        <button class="btn btn-primary" type="submit">Button</button>
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
