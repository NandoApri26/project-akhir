@extends('template.main')
@section('title', 'Dashboard')


@section('content')
    <style>
        .card img {
            width: 100%;
            height: 350px;
            object-fit: cover;
        }

    </style>

    {{-- <div class="row row-cols-1 row-cols-md-2"> --}}
    @foreach ($product as $item)
        <div class="card col-lg-3 col-6">
            <img src="{{ asset('img/' . $item->gambar) }}" class="card-img-top" alt="...">
            <div class="card-body">
                <h2 class="card-title"><b>{{ $item->product_name }}</b></h2>
                <h5 class="card-text"><b>Rp. {{ $item->product_price }}</b></h5>
                <p class="card-text">{{ $item->product_description }}</p>
                <a href="{{ url('/transaction' . $item->id) }}">
                    <div class="btn btn-warning">Buy</div>
                </a>
            </div>
        </div>
    @endforeach

@endsection
