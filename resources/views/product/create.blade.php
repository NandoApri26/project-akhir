@extends('template.main')
@section('title', 'Create Product')


@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height mt-5">
            <h3 class="ml-2"><b>Tambah Product</b></h3>
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form form-horizontal" action="{{ url('/product') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="row">

                                        <div class="col-2 mt-3">
                                            <label for="category_id">Category</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <select class="form-control" id="category_id" name="category_id">
                                                @foreach ($category as $item)
                                                    <option value="{{$item->id}}">{{$item->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="product_name">Product Name</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder=" Masukkan Judul"
                                                    id="product_name" name="product_name">
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="product_price">Product Price</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder=" Masukkan Judul"
                                                    id="product_price" name="product_price">
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="product_description">Description</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder=" Masukkan Judul"
                                                    id="product_description" name="product_description">
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="gambar">Gambar</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <input type="file" class="form-control" id="gambar" name="image">
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Button --}}
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Kirim</button>
                                            <a href="{{ url('/product') }}" class="btn btn-primary me-1 mb-1"
                                                role="button">
                                                Batal
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
