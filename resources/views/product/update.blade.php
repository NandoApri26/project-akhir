@extends('template.main')
@section('title', 'Edit Product')


@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height mt-5">
            <h3 class="ml-2"><b>Edit Product</b></h3>
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form method="POST" action="{{url ('/product/'.$product->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('GET')
                                <div class="form-body">
                                    <div class="row">
                                        {{-- Product ID--}}
                                        <div class="col-2 mt-3">
                                            <label for="category_id">Category</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <select class="form-control" id="category_id" name="category_id">
                                                    <!-- using FOREIGN ID -->
                                                @foreach ($category as $item)
                                                    <option value="{{$item->id}}">{{$item->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{-- Product Name --}}
                                        <div class="col-2 mt-3">
                                            <label for="product_name">Product Name</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control"
                                                    id="product_name" name="product_name" value="{{ $product->product_name }}">
                                            </div>
                                        </div>

                                        {{-- Product price --}}
                                        <div class="col-2 mt-3">
                                            <label for="product_price">Product Price</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control"
                                                    id="product_price" name="product_price" value="{{ $product->product_price }}">
                                            </div>
                                        </div>

                                        {{-- Product Description --}}
                                        <div class="col-2 mt-3">
                                            <label for="product_description">Product Description</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control"
                                                    id="product_description" name="product_description" value="{{ $product->product_description }}">
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="gambar">Gambar</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="file" class="form-control" placeholder="Masukkan beberapa kalimat"
                                                    id="gambar" name="image">{{ $product->gambar }}
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Kirim</button>
                                            <a href="{{ url('/product') }}" class="btn btn-primary me-1 mb-1"
                                                role="button">
                                                Batal
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection