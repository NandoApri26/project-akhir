@extends('template.main')
@section('title', 'category')


@section('content')

    <div class="card-header">
        <h3 class="card-title"><strong>Table Data category</strong></h3>
        <div class="col-12 mt-5">
            <a href="{{ url('/category/create') }}" class="btn btn-primary" role="button">
                Tambah data category
            </a>
        </div>
        @if (session('status'))
            <div class="row mt-2">
                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <table class="table table-bordered">
        <thead>
            <tr class="text-center">
                <th style="width: 10px">No</th>
                <th>Category Name</th>
                <th>Icon</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($category as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->category_name }}</td>
                    <td><img src="{{ asset('img/' . $item->icon) }}" alt="{{ $item->icon }}" width="50">
                    </td>
                    <td>
                        <form method="POST" action="{{url('/category/'.$item->id)}}">
                            @csrf
                            @method("delete")
                            <a href="{{  url('/product/') }}" class="btn btn-success btn-sm">Detail</a>
                            <a href="{{url('/category/' .$item->id).'/edit'}}" class="btn btn-info btn-sm">Edit</a>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                    {{-- <td>
                        <a class="btn btn-primary" href="#" role="button">Link</a>
                        <button class="btn btn-primary" type="submit">Button</button>
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
