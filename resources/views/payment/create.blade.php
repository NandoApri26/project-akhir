@extends('template.main')
@section('title', 'Create Payment')


@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height mt-5">
            <h3 class="ml-2"><b>Tambah Payment</b></h3>
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form form-horizontal" action="{{ url('/payment') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-2 mt-3">
                                            <label for="bank_name">Bank Name</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder=" Masukkan Judul"
                                                    id="bank_name" name="bank_name">
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="account_number">Account Number</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder=" Masukkan Judul"
                                                    id="account_number" name="account_number">
                                            </div>
                                        </div>
                                        
                                        {{-- Button --}}
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Kirim</button>
                                            <a href="{{ url('/payment') }}" class="btn btn-primary me-1 mb-1"
                                                role="button">
                                                Batal
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
