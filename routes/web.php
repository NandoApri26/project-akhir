<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\DashboardController;
use App\Category;
use App\Product;
use App\Payment;
use App\Transaction;
use App\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('template.main');
});

// Struktur Organisasi
Route::get('/category', [CategoryController::class, 'index']);
Route::get('/category/create', [CategoryController::class, 'create']);
Route::post('/category', [CategoryController::class, 'store']);
Route::delete('/category/{category}', [CategoryController::class, 'destroy']);
Route::get('/category/{category}/edit', [CategoryController::class, 'edit']);
Route::get('/category/{category}', [CategoryController::class, 'update']);

// Struktur Product
Route::get('/product', [ProductController::class, 'index']);
Route::get('/product/create', [ProductController::class, 'create']);
Route::post('/product', [ProductController::class, 'store']);
Route::delete('/product/{product}', [ProductController::class, 'destroy']);
Route::get('/product/{product}/edit', [ProductController::class, 'edit']);
Route::get('/product/{product}', [ProductController::class, 'update']);

// Struktur payment
Route::get('/payment', [PaymentController::class, 'index']);
Route::get('/payment/create', [PaymentController::class, 'create']);
Route::post('/payment', [PaymentController::class, 'store']);
Route::delete('/payment/{payment}', [PaymentController::class, 'destroy']);
Route::get('/payment/{payment}/edit', [PaymentController::class, 'edit']);
Route::get('/payment/{payment}', [PaymentController::class, 'update']);

// Struktur transaction
Route::get('/transaction', [TransactionController::class, 'index']);
Route::get('/transaction/create', [TransactionController::class, 'create']);
Route::post('/transaction', [TransactionController::class, 'store']);
Route::delete('/transaction/{transaction}', [TransactionController::class, 'destroy']);
Route::get('/transaction/{transaction}/edit', [TransactionController::class, 'edit']);
Route::get('/transaction/{transaction}', [TransactionController::class, 'update']);

// Dashboard
Route::get('/dashboard',[DashboardController::class, 'dashboard']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
