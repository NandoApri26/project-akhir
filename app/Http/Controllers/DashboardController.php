<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function Dashboard()
    {
        $product = Product::all();
        return view('dashboard.index', compact('product'));
    }
}