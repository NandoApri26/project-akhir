<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

use function Ramsey\Uuid\v1;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return view('product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('product.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $request->validate(
            [
                'category_id' => 'required',
                'product_name' => 'required',
                'product_price' => 'required',
                'product_description' => 'required',
                'image' => 'required',
            ]
        );

        $img = $request->file('image'); //mengambil file dari form
        $filename = time() . "-" . $img->getClientOriginalName(); //mengambil dan mengedit nama file dari form
        $img->move('img', $filename); //proses memasukkan image ke dalam direktori laravel

        Product::create(
            [
                "category_id" => $request->category_id,
                "product_name" => $request->product_name,
                "product_price" => $request->product_price,
                "product_description" => $request->product_description,
                "gambar" => $filename,
            ]
        );

        return redirect('/product')->with('status', 'Berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $category = Category::all();
        return view('product.update', compact('product','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // return $request;
        $request->validate(
            [
                "category_id" => 'required',
                'product_name' => 'required',
                'product_price' => 'required',
                'product_description' => 'required',
                'image' => 'required',
            ]
        );

        if ($request->image != null) {
            $img = $request->file('image');
            $filename = time() . "_" . $img->getClientOriginalName();
            $img->move('img', $filename);
            Product::where('id', $product->id)->update(
                [
                    "category_id" => $request->category_id,
                    'product_name' => $request->product_name,
                    'product_price' => $request->product_price,
                    'product_description' => $request->product_description,
                    'gambar' => $filename,
                ]
            );
        } else {
            Product::where('id', $product->id)->update(
                [
                    "category_id" => $request->category_id,
                    'product_name' => $request->product_name,
                    'product_price' => $request->product_price,
                    'product_description' => $request->product_description,
                ]
            );
        }
        return redirect('/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Product::destroy('id', $product->id);
        return redirect('/product');
    }
}
