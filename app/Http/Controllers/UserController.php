<?php

namespace App\Http\Controllers;

use App\Mail\KirimEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function login()
    {
        return view('user.login');
    }

    // Register
    public function register()
    {
        return view('user.register');
    }

    public function register_store(Request $request)
    {
        // return $request;
        $request->validate(
            [
                'name' => 'required|min',
                'email' => 'required|unique:users',
                'password' => 'required',
                'retype_password' => 'required',
            ],
            [
                'email.required' => 'Email sudah digunakan',
                'email.unique' => 'Email sudah digunakan',
            ]
        );

        User::create(
            [
                'name' => $request->fullname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                
            ]
        );
        return redirect('/login_user');
    }

    public function login_store(Request $request)
    {
        // return $request;
        $request->validate(
            [
                'email' => 'required|email',
                'password' => 'required|alpha_num|min:8|max:16'
            ],
            [
                'email.required' => 'Email harus di isi',
                'password.required' => 'Minimal angka dan huruf',
                'password.alpha_num' => 'Minimal angka dan huruf',
                'password.min' => 'Minimal 8 karakter'
            ]
        );

        Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
        if (Auth::check()) {
            $isi =  [
                'barang' => 'bunga',
                'desc' => 'bunga layu'
            ];
            Mail::to('apriando.pratamaa@gmail.com')->send(new KirimEmail($isi));
            return redirect('/home');
        } else {
            return redirect('/login_user');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/home');
    }
}
