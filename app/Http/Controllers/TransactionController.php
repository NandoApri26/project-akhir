<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Payment;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaction = Transaction::all();
        return view('transaction.index', compact('transaction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $payment = Payment::all();
        return view('transaction.create', compact('payment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $request->validate(
            [
                'payment_id' => 'required',
                'address' => 'required',
                'total_price' => 'required',
                // 'status' => 'required',
            ]
        );

        Transaction::create(
            [
                "payment_id" => $request->payment_id,
                "address" => $request->address,
                "total_price" => $request->total_price,
                // "status" => $request->status,
            ]
        );

        return redirect('/transaction')->with('status', 'Berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $payment = Payment::all();
        return view('transaction.update', compact('transaction','payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        // return $request;
        $request->validate(
            [
                "payment_id" => 'required',
                'address' => 'required',
                'total_price' => 'required',
                // 'status' => 'required',
            ]
        );

        Transaction::where('id', $transaction->id)->update(
            [
                'payment_id' => $request->payment_id,
                'address' => $request->address,
                'total_price' => $request->total_price,
                // 'status' => $request->status,
            ]
        );
        return redirect('/transaction')->with('status', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        Transaction::destroy('id', $transaction->id);
        return redirect('/transaction')->with('status',' Deleted Successfully ');
    }
}
