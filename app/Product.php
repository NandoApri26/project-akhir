<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['category_id', 'product_name', 'product_price', 'product_description', 'gambar'];

    public function category(){
        return $this -> BelongsTo ('App\Category');
    }
}
