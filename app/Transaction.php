<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = ['payment_id', 'address', 'total_price'];

    public function payment(){
        return $this -> BelongsTo ('App\Payment');
    }
}
